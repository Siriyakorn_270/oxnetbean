/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oxx;

/**
 *
 * @author My PC
 */
public class Player {
    private char name;
    private int win;
    private int lost;
    private int draw;

    public int getWin() {
        return win;
    }

    public int getLost() {
        return lost;
    }

    public int getDraw() {
        return draw;
    }

    public char getName() {
        return name;
    }
    public void win(){
        this.win++;
    }
    public void lost(){
        this.lost++;
    }
    public void draw(){
        this.draw++;
    }

    public Player(char name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Player{" + "name=" + name + '}';
    }
    
    
}
