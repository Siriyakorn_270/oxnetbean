/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oxx;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author My PC
 */
public class Game {
    private Scanner kb = new Scanner(System.in);
    private int row;
    private int col;
    Table table = null;
    Player o = null;
    Player x = null;

    public Game() {
        this.o = new Player('o');
        this.x = new Player('x');
    }
    public void run(){
        while(true){
            this.run1();
            if(Countinue()){
                return;
            }
        }
    }
    private int getRandomNumber(int min,int max){
        return (int)((Math.random()*(max - min)+ min));
    }
    public void newGame(){
        if(getRandomNumber(1,100)%2 == 0){
           this.table = new Table(o,x); 
        }else{
            this.table = new Table(x,o);
        }
    }
    public void run1(){
        this.showWelcome();
        this.table = new Table(o,x); 
        while(true){
            this.showTable();
            this.showTurn();
            this.inputRowCol();
            if(table.checkWin()){
                if(table.getWinner()!= null){
                    showWin();
                    this.showStat();
                }else{
                    showDraw();
                    
                }
                this.showStat();
                return;
            }
            
            table.switchPlayer();
        }
         
    }

    private void showDraw() {
        System.out.println("Draw!!!");
    }

    private void showWin() {
        System.out.println(table.getWinner().getName()+" "+"Winner!!!");
    }
    private void showWelcome(){
        System.out.println("Welcome to OX game.");
    }
    private void showTable(){
         char[][] data = this.table.getData();
        for(int row=0; row<data.length;row++){
            System.out.print("|");
            for(int col=0; col<data[row].length; col++){
                System.out.print(data[row][col]+"|");
        }
            System.out.println("");
        }
    }

    private void showTurn() {
        System.out.println(table.getCurrentPlayer().getName()+" "+ "Turn");
    }

    private void input() {
        while(true){
            try{
            System.out.print("Please input row col :");
            row = kb.nextInt();
            col = kb.nextInt();
            if(kb.hasNext()){
                System.out.println("Please input number row col 1-3!!");
                continue;    
            }
            return;
        }catch(InputMismatchException iE){
            kb.next();
            System.out.println("Please input number 1-3!!");
            }
        }
    }

    private void inputRowCol() {
        while(true){
            this.input();
            try{
                if(table.setRowCol(row, col)){
                    return;
                }
            }catch(ArrayIndexOutOfBoundsException e){
                System.out.println("Please input number 1-3!!");
                
            }
                
        }
    }

    private boolean Countinue() {
        while (true) {
            System.out.print("Continue Y/n : ");
                String a = kb.next();
		if (a.equals("Y")) {
                   return false;
		} else if (a.equals("n")) {
                    System.out.println("!!! GAME OVER !!!");
                    return true;
            }
	}
    }

    private void showStat() {
        System.out.println(o.getName()+""+"(win,lost,darw)" + o.getWin()+","+o.getLost()+","+o.getDraw());
        System.out.println(x.getName()+""+"(win,lost,darw)" + x.getWin()+","+x.getLost()+","+x.getDraw());
    }
    
}
