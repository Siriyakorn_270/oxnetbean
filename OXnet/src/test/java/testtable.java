/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import oxx.Table;
import oxx.Player;
/**
 *
 * @author My PC
 */
public class testtable {
    
    public testtable() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    public void testRow1Win() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table t = new Table(o,x);
        t.setRowCol(1, 1);
        t.setRowCol(1, 2);
        t.setRowCol(1, 3);
        assertEquals(true,t.checkWin());
    }
    @Test
    public void testRow2Win() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table t = new Table(o,x);
        t.setRowCol(2, 1);
        t.setRowCol(2, 2);
        t.setRowCol(2, 3);
        assertEquals(true,t.checkWin());
    }
    @Test
    public void testRow3Win() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table t = new Table(o,x);
        t.setRowCol(1, 1);
        t.setRowCol(1, 2);
        t.setRowCol(1, 3);
        assertEquals(true,t.checkWin());
    }
    @Test
     public void testCol1Win() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table t = new Table(o,x);
        t.setRowCol(1, 1);
        t.setRowCol(2, 1);
        t.setRowCol(3, 1);
        assertEquals(true,t.checkWin());
    }
     @Test
     public void testCol2Win() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table t = new Table(o,x);
        t.setRowCol(1, 2);
        t.setRowCol(2, 2);
        t.setRowCol(3, 2);
        assertEquals(true,t.checkWin());
    }
     @Test
     public void testCol3Win() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table t = new Table(o,x);
        t.setRowCol(1, 3);
        t.setRowCol(2, 3);
        t.setRowCol(3, 3);
        assertEquals(true,t.checkWin());
    }
     @Test
    public void testDia1Win() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table t = new Table(o,x);
        t.setRowCol(1, 1);
        t.setRowCol(2, 2);
        t.setRowCol(3, 3);
        assertEquals(true,t.checkWin());
    }
    @Test
    public void testDia2Win() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table t = new Table(o,x);
        t.setRowCol(1, 3);
        t.setRowCol(2, 2);
        t.setRowCol(3, 1);
        assertEquals(true,t.checkWin());
    }
    @Test
    public void testSwitchPlayer(){
        Player o = new Player('o');
        Player x = new Player('x');
        Table t = new Table(o,x);
        t.switchPlayer();
        assertEquals('x',t.getCurrentPlayer().getName());
    }
    @Test
    public void testCountDraw(){
        Player p = new Player('o');
            p.draw();
            assertEquals(1,p.getDraw());
     }
    @Test
    public void testCountWin(){
        Player p = new Player('o');
            p.win();
            assertEquals(1,p.getWin());
     }
    @Test
    public void testCountLost(){
        Player p = new Player('o');
            p.lost();
            assertEquals(1,p.getLost());
     }
}
